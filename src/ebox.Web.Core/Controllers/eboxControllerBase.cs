using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ebox.Controllers
{
    public abstract class eboxControllerBase: AbpController
    {
        protected eboxControllerBase()
        {
            LocalizationSourceName = eboxConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
