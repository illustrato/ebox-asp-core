using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using ebox.Employees;

namespace ebox.Items
{
    [Table("abp_equipment_unit")]
    public class Unit : Entity<ulong>
    {
        public const int MaxSerialLength = 45;
        public virtual Equipment Equipment { get; protected set; }
        public virtual Guid EquipmentId { get; protected set; }
        public virtual DateTime Date { get; protected set; }
        public virtual bool IsFit { get; protected set; }
        
        [StringLength(MaxSerialLength)]
        public virtual string ProductSerial { get; protected set; }
        public virtual Employee Employee {get; protected set;}
        public virtual long? UserId {get; protected set;}
        public virtual DateTime? UsageDate { get; protected set; }
        [ForeignKey("UnitId")] 
        public virtual ICollection<Fault> Faults { get; protected set; }

        protected Unit()
        {
        }
        public static Unit Create(
            ulong id,
            Guid equipmentId,
            String serial
        )
        {
            var @equipmentUnit = new Unit
            {
                Id = id,
                EquipmentId = equipmentId,
                ProductSerial = serial,
                IsFit = true,
                Date = DateTime.Now
            };
            return @equipmentUnit;
        }
        public void Fit(bool state){
            IsFit = state;
        }

        public void Delegate(long? userId){
            UserId = userId;
            UsageDate = DateTime.Now;
        }
    }
}