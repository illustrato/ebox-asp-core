using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using ebox.Authorization.Users;
using ebox.Employees;

namespace ebox.Items
{
    [Table("abp_unit_fault")]
    public class Fault : Entity<long>
    {
        public virtual Employee Employee { get; protected set; }
        public virtual long UserId { get; set; }
        public virtual Unit Unit { get; protected set; }
        public virtual ulong UnitId { get; set; }
        public virtual DateTime DateReported { get; protected set; }
        public virtual DateTime? DateAddended { get; protected set; }

        public virtual string Description { get; protected set; }
        public virtual bool Attended { get; protected set;}

        protected Fault(){

        }
        public static Fault Create(long userId, ulong unitId, string description = null){
            return new Fault{
                UserId = userId,
                UnitId = unitId,
                DateReported = DateTime.Now,
                Description = description,
                Attended = false
            };
        }

        public void Attend(){
            DateAddended = DateTime.Now;
            Attended = true;
        }
    }
}