using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Services;
using ebox.Employees;

namespace ebox.Items {
    public interface IEquipmentManager : IDomainService {

        Task<Guid> CreateAsync (Equipment @equipment);


    }
}