using System;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Abp.UI;

namespace ebox.Items
{
    public class EquipmentManager : IEquipmentManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Equipment, Guid> _equipmentRepository;
        public EquipmentManager(
             IRepository<Equipment, Guid> equipmentRepository
        )
        {
            _equipmentRepository = equipmentRepository;
            EventBus = NullEventBus.Instance;
        }

        public async Task<Guid> CreateAsync(Equipment @equipment)
        {
            return await _equipmentRepository.InsertAndGetIdAsync(@equipment);
        }
    }
}