using System.Threading.Tasks;
using Abp.Domain.Services;

namespace ebox.Items
{
    public interface IUnitManager : IDomainService
    {
         Task CreateAsync(Unit @unit);
         Task<ulong> GenerateId();
    }
}