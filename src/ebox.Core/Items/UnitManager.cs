using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Microsoft.EntityFrameworkCore;

namespace ebox.Items
{
    public class UnitManager : IUnitManager
    {
        public IEventBus EventBus { get; set; }
        public virtual IQueryable<Unit> Units { get => _unitRepo.GetAll(); }
        private readonly IRepository<Unit, ulong> _unitRepo;
        public UnitManager(
            IRepository<Unit, ulong> unitRepo
        ){
            _unitRepo = unitRepo;

            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Unit unit)
        {
            await _unitRepo.InsertAsync(unit);
        }
        public async Task<ulong> GenerateId(){
            var ids = await _unitRepo.GetAll().Select(e => e.Id).ToListAsync();
            ulong id = 1;
            while(ids.Contains(id)){
                id++;
            }
            return id;
        }
    }
}