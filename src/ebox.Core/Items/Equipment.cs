using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ebox.Items {
    [Table ("abp_equipment")]
    public class Equipment : FullAuditedEntity<Guid>, IMustHaveTenant {
        public const int MaxTitleLength = 50;
        public const int MaxDescriptionLength = 1024;
        public virtual int TenantId { get; set; }

        [Required]
        [StringLength (MaxTitleLength)]
        public virtual string Title { get; protected set; }

        [StringLength (MaxDescriptionLength)]
        public virtual string Specs { get; protected set; }
        [StringLength (MaxDescriptionLength)]
        public virtual string Description { get; protected set; }
        [Required]
        public virtual EquipmentType EquipmentType { get; protected set; }
        public virtual byte TypeId { get; protected set; }

        public virtual string DepartmentId { get; protected set; }
        public virtual string Image { get; protected set;}

        [ForeignKey("EquipmentId")] 
        public virtual ICollection<Unit> Units { get; protected set; }

        /// <summary>
        /// We don't make constructor public and forcing to create equipment using <see cref="Create"/> method.
        /// But constructor can not be private since it's used by EntityFramework.
        /// Thats why we did it protected.
        /// </summary>
        protected Equipment () {
        }
        public static Equipment Create (
            int tenantId, 
            string title, 
            byte typeId,
            string departmentId,
            string specs,
            string desc = null) 
            {
            var @equipment = new Equipment {
                Id = Guid.NewGuid (),
                TenantId = tenantId,
                Title = title,
                Specs = specs,
                Description = desc,
                TypeId = typeId,
                DepartmentId = departmentId
            };
            @equipment.Units = new Collection<Unit>();
            return @equipment;
        }
        public void SetImage(string image = null){
            Image = image;
        }
    }
}