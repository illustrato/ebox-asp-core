using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace ebox.Items
{
    [Table("abp_equipment_type")]
    public class EquipmentType : Entity<byte>
    {
        public const int MaxTitleLength = 35;
        public const int MaxDescriptionLength = 1024;
        private static byte count = 0;

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override byte Id { get; set;}

        [Required]
        [StringLength (MaxTitleLength)]
        public virtual string Title { get; protected set; }

        [StringLength (MaxDescriptionLength)]
        public virtual string Description { get; protected set; }
        [StringLength(MaxTitleLength)]
        public virtual string FontIcon { get; protected set;}

        [ForeignKey ("TypeId")]
        public virtual ICollection<Equipment> Equipments { get; protected set; }
        public EquipmentType(string title){
            Id = ++count;
            this.Title = title;
        }
        public EquipmentType(string title, string fontIcon){
            Id = ++count;
            this.Title = title;
            this.FontIcon = fontIcon;
        }
        public static EquipmentType Create(byte id, string title, string desc = null){
            var @type = new EquipmentType(title){
                Id = id,
                Description = desc
            };
            @type.Equipments = new Collection<Equipment>();
            return @type;
        }

    }
}