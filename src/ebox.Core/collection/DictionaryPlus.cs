using System.Collections.Generic;

namespace ebox.collection
{
    public class DictionaryPlus<TType> : Dictionary<string,TType>
    {
        public TType Value { get; set; }
        public DictionaryPlus(){

        }
        public DictionaryPlus(TType value){
            this.Value = value;
        }
        public override string ToString()
        {
            return (Value.ToString() ?? string.Empty).ToString();
        }
    }
}