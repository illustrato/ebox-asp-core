using System.Collections.Generic;

namespace ebox.collection
{
    public class RecursiveDictionary : Dictionary<string, RecursiveDictionary>
    {
        public object Value { get; set; }
        public RecursiveDictionary(){

        }
        public RecursiveDictionary(object value){
            this.Value = value;
        }

        public override string ToString()
        {
            return (Value ?? string.Empty).ToString();
        }
    }
}