using Abp.Domain.Entities.Auditing;
using ebox.Authorization.Users;

namespace ebox.MultiTenancy
{
    public class AdminTenant : FullAuditedEntity<string>
    {
        public virtual long UserId { get; protected set; }
        public virtual Admin Admin { get; protected set; }
        public virtual int TenantId { get; protected set; }
        public virtual Tenant Tenant { get; protected set;}
        protected AdminTenant(){

        }
        public static AdminTenant Create(long userId, int tenantId)
        {
            return new AdminTenant{
                Id = "" + userId + "" + tenantId,
                UserId = userId,
                TenantId = tenantId
            };
        }
    }
}