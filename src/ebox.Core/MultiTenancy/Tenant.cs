﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.MultiTenancy;
using ebox.Authorization.Users;
using ebox.Departments;

namespace ebox.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        [ForeignKey("TenantId")] 
        public virtual ICollection<OrgDept> Departments { get; protected set; }
        [ForeignKey("TenantId")] 
        public virtual ICollection<AdminTenant> Admins { get; protected set; }
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
