﻿namespace ebox
{
    public class eboxConsts
    {
        public const string LocalizationSourceName = "ebox";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
