using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using ebox.Employees;
using ebox.Items;
using ebox.MultiTenancy;

namespace ebox.Departments
{
    [Table("abp_org_dept")]
    public class OrgDept : Entity<string>, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public Tenant Tenant { get; set;}
        [Required]
        public virtual string DepartmentId { get; protected set;}
        public virtual Department Department { get; protected set; }
        public virtual string Description { get; protected set; }

        public virtual bool IsDeleted { get; protected set;}
        public virtual DateTime? DeletionTime { get; protected set;}
        public virtual long? DeleterUserId { get; protected set; }
        
        [ForeignKey("DepartmentId")] 
        public virtual ICollection<Employee> Employees {get; protected set;}
        [ForeignKey("DepartmentId")] 
        public virtual ICollection<Equipment> Equipment {get; protected set;}
        protected OrgDept()
        {
        }
        public static OrgDept Create(int tenantId, string departmentId, string description = null){
            return new OrgDept{
                Id = departmentId + tenantId,
                TenantId = tenantId,
                DepartmentId = departmentId,
                Description = description
            };
        }
        public void Delete(long? uid){
            IsDeleted = true;
            DeletionTime = DateTime.Now;
            DeleterUserId = uid;
        }
        public void Redeem(string departmentId, int tenantId){
            Id = departmentId + tenantId;
            TenantId = tenantId;
            IsDeleted = false;
            DeletionTime = null;
            DeleterUserId = null;
        }
    }
}