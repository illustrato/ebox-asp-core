using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace ebox.Departments
{
    [Table("abp_department")]
    public class Department : FullAuditedEntity<string>
    {
        public const int MaxIdLength = 2;
        public const int MaxTitleLength = 13;
        [StringLength(MaxIdLength)]
        public override string Id { get; set; }
        [Required]
        [StringLength(MaxTitleLength)]
        public virtual string DeptName { get; protected set;}

        [StringLength(MaxTitleLength)]
        public virtual string FontIcon { get; protected set;}

        [ForeignKey("DepartmentId")] 
        public virtual ICollection<OrgDept> Organizations { get; protected set; }
        public Department(string id, string deptName, string fontIcon = null)
        {
            this.Id = id;
            this.DeptName = deptName;
            this.FontIcon = fontIcon;
        }
    }
}