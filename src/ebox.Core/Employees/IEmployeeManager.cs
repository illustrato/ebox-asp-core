using System.Threading.Tasks;
using Abp.Domain.Services;
using Microsoft.AspNetCore.Identity;

namespace ebox.Employees
{
    public interface IEmployeeManager : IDomainService
    {
        Task<Employee> GetAsync(long id);
        Task CreateAsync(Employee @employee);
        Task UpdateAsync(Employee @employee);
    }
}