using System.Threading.Tasks;
using Abp.Domain.Services;

namespace ebox.Employees
{
    public interface IManagerManager : IDomainService
    {
         
        Task<Manager> GetAsync(long id);
        Task CreateAsync(Manager @manager);
        Task EditAsync(Manager @manager);
        Task DeleteAsync(Manager @manager);
    }
}