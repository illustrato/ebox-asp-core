using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;

namespace ebox.Employees
{
    public class ManagerManager : IManagerManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Manager, long> _managerRepository;
        public ManagerManager(
             IRepository<Manager, long> managerRepository
        )
        {
            _managerRepository = managerRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Manager manager)
        {
             await _managerRepository.InsertAsync(@manager);
        }

        public async Task DeleteAsync(Manager manager)
        {
             await _managerRepository.DeleteAsync(@manager);
        }

        public async Task EditAsync(Manager manager)
        {
            await _managerRepository.UpdateAsync(@manager);
        }

        public async Task<Manager> GetAsync(long id)
        {
            return await _managerRepository.GetAsync(id);
        }
    }
}