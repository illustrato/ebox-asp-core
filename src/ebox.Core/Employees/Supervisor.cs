using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ebox.Employees
{
    [Table("abp_supervisor")]
    public class Supervisor : Entity<long>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override long Id {get;set;}
        public virtual char TypeId { get; protected set;}

        [ForeignKey("Id")]
        public virtual Employee Employee { get; protected set; }

        protected Supervisor()
        {

        }

        public static Supervisor Create(long id)
        {
            return new Supervisor{Id = id, TypeId = 'S'};
        }
    }
}