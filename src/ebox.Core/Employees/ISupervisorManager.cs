using System.Threading.Tasks;
using Abp.Domain.Services;

namespace ebox.Employees
{
    public interface ISupervisorManager : IDomainService
    {
        Task<Supervisor> GetAsync(long id);
        Task CreateAsync(Supervisor @supervisor);
        Task EditAsync(Supervisor @supervisor);
        Task DeleteAsync(Supervisor @supervisor);
    }
}