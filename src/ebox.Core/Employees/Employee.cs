using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using ebox.Authorization.Users;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ebox.Items;

namespace ebox.Employees
{
    [Table("abp_employee")]
    public class Employee : Entity<long>
    {
        public const int MaxIdNoLength = 13;
        [ForeignKey("Id")]
        public virtual User User { get; protected set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override long Id { get; set; }
        [StringLength(MaxIdNoLength)]
        public virtual string IdNo { get; protected set; }
        public virtual string DepartmentId { get; protected set; }
        
        [ForeignKey("UserId")] 
        public virtual ICollection<Unit> Units { get; protected set; }
        [ForeignKey("UserId")] 
        public virtual ICollection<Fault> Faults { get; protected set; }

        protected Employee()
        {
            
        }
        
        public static Employee Create(
           long userId, 
           string idNo,
           string departmentId
       )
        {
            var @employee = new Employee
            {
                Id = userId,
                IdNo = idNo,
                DepartmentId = departmentId
            };
            return @employee;
        }
        public void Edit(
            string departmentId,
            string idNo
        )
        {
            IdNo = idNo;
            DepartmentId = departmentId;
        }
        public void RegisterDepartment(string registration){
            DepartmentId = registration;
        }
        public void TerminateRegistration(){
            DepartmentId = null;
        }
    }
}