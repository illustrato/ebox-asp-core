using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ebox.Employees
{
    [Table("abp_manager")]
    public class Manager : Entity<long>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override long Id {get;set;}
        public virtual char TypeId { get; protected set;}

        [ForeignKey("Id")]
        public virtual Employee Employee { get; protected set; }
        protected Manager()
        {

        }
        public static Manager Create(long id){
            return new Manager{Id = id, TypeId = 'M'};
        }
    }
}