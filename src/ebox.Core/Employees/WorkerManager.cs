using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;

namespace ebox.Employees
{
    public class WorkerManager : IWorkerManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Worker, long> _workerRepository;
        public WorkerManager(
             IRepository<Worker, long> workerRepository
        )
        {
            _workerRepository = workerRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Worker worker)
        {
             await _workerRepository.InsertAsync(@worker);
        }

        public async Task DeleteAsync(Worker worker)
        {
             await _workerRepository.DeleteAsync(@worker);
        }

        public async Task EditAsync(Worker worker)
        {
            await _workerRepository.UpdateAsync(@worker);
        }

        public async Task<Worker> GetAsync(long id)
        {
            return await _workerRepository.GetAsync(id);
        }
    }
}