using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace ebox.Employees
{
    [Table("abp_worker")]
    public class Worker : Entity<long>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override long Id {get;set;}
        public virtual char TypeId { get; protected set;}

        [ForeignKey("Id")]
        public virtual Employee Employee { get; protected set; }
        protected Worker()
        {
            
        }
        public static Worker Create(long id)
        {
            return new Worker{Id = id, TypeId = 'W'};
        }
    }
}