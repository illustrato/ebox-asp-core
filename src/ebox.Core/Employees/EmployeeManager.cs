using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Events.Bus;
using Abp.UI;
using Microsoft.EntityFrameworkCore;

namespace ebox.Employees
{
    public class EmployeeManager : IEmployeeManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Employee, long> _empRepo;
        private readonly IRepository<Manager, long> _managerRepo;
        private readonly IRepository<Supervisor, long> _supervisorRepo;
        private readonly IRepository<Worker, long> _workerRepo;

        public EmployeeManager(
             IRepository<Employee, long> employeeRepository,
             IRepository<Manager, long> managerRepository,
             IRepository<Supervisor, long> supervisorRepository,
             IRepository<Worker, long> workerRepository
        )
        {
            _empRepo = employeeRepository;
            _managerRepo = managerRepository;
            _supervisorRepo = supervisorRepository;
            _workerRepo = workerRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Employee employee)
        {
            await _empRepo.InsertAsync(@employee);
        }

        public async Task UpdateAsync(Employee employee)
        {
            await _empRepo.UpdateAsync(@employee);
        }

        public async Task<Employee> GetAsync(long id)
        {
            var emp = await _empRepo.GetAllIncluding(x => x.User).FirstOrDefaultAsync(x => x.Id == id);

            if (emp == null)
            {
                throw new EntityNotFoundException(typeof(Employee), id);
            }
            return emp;
        }
    }
}