using System.Threading.Tasks;
using Abp.Domain.Services;

namespace ebox.Employees
{
    public interface IWorkerManager  : IDomainService
    {
          Task<Worker> GetAsync(long id);
        Task CreateAsync(Worker @worker);
        Task EditAsync(Worker @worker);
        Task DeleteAsync(Worker @worker);
    }
}