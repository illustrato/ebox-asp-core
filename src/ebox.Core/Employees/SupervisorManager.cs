using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Events.Bus;

namespace ebox.Employees
{
    public class SupervisorManager : ISupervisorManager
    {
        public IEventBus EventBus { get; set; }
        private readonly IRepository<Supervisor, long> _supervisorRepository;
        public SupervisorManager(
             IRepository<Supervisor, long> supervisorRepository
        )
        {
            _supervisorRepository = supervisorRepository;
            EventBus = NullEventBus.Instance;
        }
        public async Task CreateAsync(Supervisor supervisor)
        {
            await _supervisorRepository.InsertAsync(@supervisor);
        }

        public async Task DeleteAsync(Supervisor supervisor)
        {
           await _supervisorRepository.DeleteAsync(@supervisor);
        }

        public async Task EditAsync(Supervisor supervisor)
        {
             await _supervisorRepository.UpdateAsync(@supervisor);
        }

        public async Task<Supervisor> GetAsync(long id)
        {
           return await _supervisorRepository.GetAsync(id);
        }
    }
}