using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using ebox.MultiTenancy;

namespace ebox.Authorization.Users
{
    [Table("abp_admin")]
    public class Admin : Entity<long>
    {
        [ForeignKey("Id")]
        public virtual User User { get; protected set; }
        public override long Id { get; set; }
        [ForeignKey("UserId")] 
        public virtual ICollection<AdminTenant> Tenants { get; protected set; }

        protected Admin(){

        }
        public static Admin Create(long id)
        {
            return new Admin{
                Id = id,
                Tenants = new Collection<AdminTenant>()
            };
        }
    }
}