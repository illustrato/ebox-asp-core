﻿using System;
using System.Collections.Generic;
using Abp.Authorization.Users;
using Abp.Extensions;

namespace ebox.Authorization.Users
{
    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        public User(){

        }

        public User(string uname, string name, string surname, string emailAdress, string phoneNumber)
        {
            UserName = uname;
            Name = name;
            Surname = surname;
            EmailAddress = emailAdress;
            PhoneNumber = phoneNumber;
        }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }
        public static User CreateAdmin(string username, string emailAddress)
        {
            var user = new User
            {
                UserName = username,
                Name = "Admin",
                Surname = "Admin",
                EmailAddress = emailAddress,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}
