﻿using Abp.Authorization;
using ebox.Authorization.Roles;
using ebox.Authorization.Users;

namespace ebox.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
