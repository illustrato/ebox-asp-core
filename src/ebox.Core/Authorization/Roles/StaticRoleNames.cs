namespace ebox.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string Manager = "Manager";
            public const string Supervisor = "Supervisor";
            public const string Worker = "Worker";
        }
    }
}
