﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace ebox.Authorization
{
    public class eboxAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Departments,L("Departments"));
            context.CreatePermission(PermissionNames.Pages_Staff, L("Staff"));
            context.CreatePermission(PermissionNames.Pages_Equipment, L("Equipment"));
            context.CreatePermission(PermissionNames.Pages_Stats_New, L("Stats_New"));
            context.CreatePermission(PermissionNames.Pages_Your_Requests, L("Your_Requests"));
            context.CreatePermission(PermissionNames.Pages_Staff_Requests, L("Staff_Requests"));
            context.CreatePermission(PermissionNames.Pages_Repo, L("Repo"));
            context.CreatePermission(PermissionNames.Pages_Acquisitions, L("Acquisitions"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, eboxConsts.LocalizationSourceName);
        }
    }
}
