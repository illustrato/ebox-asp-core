﻿namespace ebox.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Users = "Pages.Users";
        public const string Pages_Roles = "Pages.Roles";
        public const string Pages_Departments = "Pages.Departments";
        public const string Pages_Staff = "Pages.Staff";
        public const string Pages_Equipment = "Pages.Equipment";
        public const string Pages_Stats_New = "Pages.Stats_New";
        public const string Pages_Repo = "Pages.Repo";
        public const string Pages_Your_Requests = "Pages.Your_Requests";
        public const string Pages_Staff_Requests = "Pages.Staff_Requests";
        public const string Pages_Acquisitions = "Pages.Acquisitions";
    }
}
