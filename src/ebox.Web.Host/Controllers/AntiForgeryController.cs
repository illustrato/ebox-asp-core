using Microsoft.AspNetCore.Antiforgery;
using ebox.Controllers;

namespace ebox.Web.Host.Controllers
{
    public class AntiForgeryController : eboxControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
