﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class deptEquipment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee");

            migrationBuilder.DropTable(
                name: "abp_employee_type");

            migrationBuilder.DropIndex(
                name: "IX_abp_employee_TypeId",
                table: "abp_employee");

            migrationBuilder.DropIndex(
                name: "IX_abp_employee_Id_TypeId",
                table: "abp_employee");

            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "abp_employee");

            migrationBuilder.AddColumn<string>(
                name: "DepartmentId",
                table: "abp_equipment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "abp_equipment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_equipment_DepartmentId",
                table: "abp_equipment",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_equipment_abp_org_dept_DepartmentId",
                table: "abp_equipment",
                column: "DepartmentId",
                principalTable: "abp_org_dept",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_equipment_abp_org_dept_DepartmentId",
                table: "abp_equipment");

            migrationBuilder.DropIndex(
                name: "IX_abp_equipment_DepartmentId",
                table: "abp_equipment");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "abp_equipment");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "abp_equipment");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "abp_org_dept",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TypeId",
                table: "abp_employee",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "abp_employee_type",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Description = table.Column<string>(maxLength: 13, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_employee_type", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abp_employee_TypeId",
                table: "abp_employee",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee",
                column: "TypeId",
                principalTable: "abp_employee_type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.CreateIndex(
                name: "IX_abp_employee_Id_TypeId",
                table: "abp_employee",
                columns: new[] { "Id", "TypeId" },
                unique: true);
        }
    }
}
