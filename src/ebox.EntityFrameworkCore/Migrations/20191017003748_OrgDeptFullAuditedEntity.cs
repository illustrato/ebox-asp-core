﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class OrgDeptFullAuditedEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Online",
                table: "abp_org_dept",
                newName: "IsDeleted");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "abp_org_dept",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "abp_org_dept",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "abp_org_dept",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "abp_org_dept");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "abp_org_dept");

            migrationBuilder.RenameColumn(
                name: "IsDeleted",
                table: "abp_org_dept",
                newName: "Online");
        }
    }
}
