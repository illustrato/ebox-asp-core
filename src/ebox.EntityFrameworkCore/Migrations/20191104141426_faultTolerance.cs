﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class faultTolerance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "abp_faulty");

            migrationBuilder.AddColumn<DateTime>(
                name: "UsageDate",
                table: "abp_equipment_unit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "abp_equipment_unit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FontIcon",
                table: "abp_equipment_type",
                maxLength: 35,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Specs",
                table: "abp_equipment",
                maxLength: 1024,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "abp_unit_fault",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<long>(nullable: false),
                    UnitId = table.Column<decimal>(nullable: false),
                    DateReported = table.Column<DateTime>(nullable: false),
                    DateAddended = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Attended = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_unit_fault", x => x.Id);
                    table.ForeignKey(
                        name: "FK_abp_unit_fault_abp_equipment_unit_UnitId",
                        column: x => x.UnitId,
                        principalTable: "abp_equipment_unit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_abp_unit_fault_abp_employee_UserId",
                        column: x => x.UserId,
                        principalTable: "abp_employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abp_equipment_unit_UserId",
                table: "abp_equipment_unit",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_unit_fault_UnitId",
                table: "abp_unit_fault",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_unit_fault_UserId",
                table: "abp_unit_fault",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_equipment_unit_abp_employee_UserId",
                table: "abp_equipment_unit",
                column: "UserId",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_equipment_unit_abp_employee_UserId",
                table: "abp_equipment_unit");

            migrationBuilder.DropTable(
                name: "abp_unit_fault");

            migrationBuilder.DropIndex(
                name: "IX_abp_equipment_unit_UserId",
                table: "abp_equipment_unit");

            migrationBuilder.DropColumn(
                name: "UsageDate",
                table: "abp_equipment_unit");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "abp_equipment_unit");

            migrationBuilder.DropColumn(
                name: "FontIcon",
                table: "abp_equipment_type");

            migrationBuilder.DropColumn(
                name: "Specs",
                table: "abp_equipment");

            migrationBuilder.CreateTable(
                name: "abp_faulty",
                columns: table => new
                {
                    Id = table.Column<decimal>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    EquipmentId = table.Column<Guid>(nullable: false),
                    ProbDesc = table.Column<string>(nullable: true),
                    WorkerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_faulty", x => x.Id);
                    table.ForeignKey(
                        name: "FK_abp_faulty_abp_equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "abp_equipment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_abp_faulty_abp_worker_WorkerId",
                        column: x => x.WorkerId,
                        principalTable: "abp_worker",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abp_faulty_EquipmentId",
                table: "abp_faulty",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_faulty_WorkerId",
                table: "abp_faulty",
                column: "WorkerId");
        }
    }
}
