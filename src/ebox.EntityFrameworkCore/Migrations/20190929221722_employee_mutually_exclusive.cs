﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class employee_mutually_exclusive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_manager_abp_employee_Id",
                table: "abp_manager");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_supervisor_abp_employee_Id",
                table: "abp_supervisor");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_worker_abp_employee_Id",
                table: "abp_worker");

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_worker",
                nullable: true);

            migrationBuilder.AddColumn<char>(
                name: "TypeId",
                table: "abp_worker",
                nullable: false,
                defaultValue: 'W');

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_supervisor",
                nullable: true);

            migrationBuilder.AddColumn<char>(
                name: "TypeId",
                table: "abp_supervisor",
                nullable: false,
                defaultValue: 'S');

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_manager",
                nullable: true);

            migrationBuilder.AddColumn<char>(
                name: "TypeId",
                table: "abp_manager",
                nullable: false,
                defaultValue: 'M');

            migrationBuilder.AddColumn<char>(
                name: "TypeId",
                table: "abp_employee",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "abp_employee_type",
                columns: table => new
                {
                    Id = table.Column<char>(nullable: false),
                    Description = table.Column<string>(maxLength: 13, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_employee_type", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abp_worker_EmployeeId",
                table: "abp_worker",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_supervisor_EmployeeId",
                table: "abp_supervisor",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_manager_EmployeeId",
                table: "abp_manager",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_employee_TypeId",
                table: "abp_employee",
                column: "TypeId");
                
            migrationBuilder.AddForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee",
                column: "TypeId",
                principalTable: "abp_employee_type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.CreateIndex(
                name: "IX_abp_employee_Id_TypeId",
                table: "abp_employee",
                columns: new[] { "Id", "TypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_manager_Id_TypeId",
                table: "abp_manager",
                columns: new[] { "Id", "TypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_supervisor_Id_TypeId",
                table: "abp_supervisor",
                columns: new[] { "Id", "TypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_worker_Id_TypeId",
                table: "abp_worker",
                columns: new[] { "Id", "TypeId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_manager_abp_employee_EmployeeId",
                table: "abp_manager",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_supervisor_abp_employee_EmployeeId",
                table: "abp_supervisor",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_worker_abp_employee_EmployeeId",
                table: "abp_worker",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_manager_abp_employee_EmployeeId",
                table: "abp_manager");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_supervisor_abp_employee_EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_worker_abp_employee_EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropTable(
                name: "abp_employee_type");

            migrationBuilder.DropIndex(
                name: "IX_abp_worker_EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropIndex(
                name: "IX_abp_supervisor_EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropIndex(
                name: "IX_abp_manager_EmployeeId",
                table: "abp_manager");

            migrationBuilder.DropIndex(
                name: "IX_abp_employee_TypeId",
                table: "abp_employee");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "abp_worker");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "abp_supervisor");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_manager");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "abp_manager");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "abp_employee");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_manager_abp_employee_Id",
                table: "abp_manager",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_supervisor_abp_employee_Id",
                table: "abp_supervisor",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_worker_abp_employee_Id",
                table: "abp_worker",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
