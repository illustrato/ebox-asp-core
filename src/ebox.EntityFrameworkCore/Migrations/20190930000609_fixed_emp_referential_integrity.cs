﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class fixed_emp_referential_integrity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_manager_abp_employee_EmployeeId",
                table: "abp_manager");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_supervisor_abp_employee_EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_worker_abp_employee_EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropIndex(
                name: "IX_abp_worker_EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropIndex(
                name: "IX_abp_supervisor_EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropIndex(
                name: "IX_abp_manager_EmployeeId",
                table: "abp_manager");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_worker");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_supervisor");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "abp_manager");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_manager_abp_employee_Id",
                table: "abp_manager",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_supervisor_abp_employee_Id",
                table: "abp_supervisor",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_worker_abp_employee_Id",
                table: "abp_worker",
                column: "Id",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_manager_abp_employee_Id",
                table: "abp_manager");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_supervisor_abp_employee_Id",
                table: "abp_supervisor");

            migrationBuilder.DropForeignKey(
                name: "FK_abp_worker_abp_employee_Id",
                table: "abp_worker");

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_worker",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_supervisor",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "EmployeeId",
                table: "abp_manager",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_worker_EmployeeId",
                table: "abp_worker",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_supervisor_EmployeeId",
                table: "abp_supervisor",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_manager_EmployeeId",
                table: "abp_manager",
                column: "EmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_manager_abp_employee_EmployeeId",
                table: "abp_manager",
                column: "EmployeeId",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_supervisor_abp_employee_EmployeeId",
                table: "abp_supervisor",
                column: "EmployeeId",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_worker_abp_employee_EmployeeId",
                table: "abp_worker",
                column: "EmployeeId",
                principalTable: "abp_employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
