﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class introduced_departments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "abp_employee_type",
                maxLength: 13,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 13,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "TypeId",
                table: "abp_employee",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "abp_department",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 2, nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DeptName = table.Column<string>(maxLength: 13, nullable: false),
                    FontIcon = table.Column<string>(maxLength: 13, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_department", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "abp_org_dept",
                columns: table => new
                {
                    Id = table.Column<decimal>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<string>(nullable: false),
                    Online = table.Column<bool>(nullable: false, defaultValue: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_abp_org_dept", x => x.Id);
                    table.ForeignKey(
                        name: "FK_abp_org_dept_abp_department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "abp_department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_abp_org_dept_AbpTenants_TenantId",
                        column: x => x.TenantId,
                        principalTable: "AbpTenants",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_abp_org_dept_DepartmentId",
                table: "abp_org_dept",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_abp_org_dept_TenantId",
                table: "abp_org_dept",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee",
                column: "TypeId",
                principalTable: "abp_employee_type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee");

            migrationBuilder.DropTable(
                name: "abp_org_dept");

            migrationBuilder.DropTable(
                name: "abp_department");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "abp_employee_type",
                maxLength: 13,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 13);

            migrationBuilder.AlterColumn<string>(
                name: "TypeId",
                table: "abp_employee",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_abp_employee_abp_employee_type_TypeId",
                table: "abp_employee",
                column: "TypeId",
                principalTable: "abp_employee_type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
