﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class OrgDeptIdString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_abp_org_dept",
                table: "abp_org_dept");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "abp_org_dept",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddPrimaryKey("PK_abp_org_dept","abp_org_dept","ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_abp_org_dept",
                table: "abp_org_dept");

            migrationBuilder.AlterColumn<decimal>(
                name: "Id",
                table: "abp_org_dept",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey("PK_abp_org_dept","abp_org_dept","ID");
        }
    }
}
