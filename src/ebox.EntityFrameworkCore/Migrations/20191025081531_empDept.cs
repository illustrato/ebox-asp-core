﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ebox.Migrations
{
    public partial class empDept : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DepartmentId",
                table: "abp_employee",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_abp_employee_DepartmentId",
                table: "abp_employee",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_abp_employee_abp_org_dept_DepartmentId",
                table: "abp_employee",
                column: "DepartmentId",
                principalTable: "abp_org_dept",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_abp_employee_abp_org_dept_DepartmentId",
                table: "abp_employee");

            migrationBuilder.DropIndex(
                name: "IX_abp_employee_DepartmentId",
                table: "abp_employee");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "abp_employee");
        }
    }
}
