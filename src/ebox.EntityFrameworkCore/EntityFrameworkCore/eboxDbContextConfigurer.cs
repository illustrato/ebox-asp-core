using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ebox.EntityFrameworkCore
{
    public static class eboxDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<eboxDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<eboxDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
