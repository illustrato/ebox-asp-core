﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ebox.Configuration;
using ebox.Web;

namespace ebox.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class eboxDbContextFactory : IDesignTimeDbContextFactory<eboxDbContext>
    {
        public eboxDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<eboxDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            eboxDbContextConfigurer.Configure(builder, configuration.GetConnectionString(eboxConsts.ConnectionStringName));

            return new eboxDbContext(builder.Options);
        }
    }
}
