﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ebox.Authorization.Roles;
using ebox.Authorization.Users;
using ebox.MultiTenancy;
using ebox.Employees;
using ebox.Items;
using ebox.Departments;

namespace ebox.EntityFrameworkCore
{
    public class eboxDbContext : AbpZeroDbContext<Tenant, Role, User, eboxDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<OrgDept> OrgDepts {get; set;}
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Worker> Workers { get; set; }
        public virtual DbSet<Manager> Managers { get; set; }
        public virtual DbSet<Supervisor> Supervisors { get; set; }
        public virtual DbSet<Equipment> EquipmentItems { get; set; }
        public virtual DbSet<EquipmentType> EquipmentTypes { get; set; }
        public virtual DbSet<Unit> Units {get; set;}
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<AdminTenant> AdminTenants {get; set;}
        public virtual DbSet<Fault> Faults {get; set;}
        
        public eboxDbContext(DbContextOptions<eboxDbContext> options)
            : base(options)
        {
        }
    }
}
