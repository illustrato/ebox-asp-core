using System.Linq;
using ebox.Items;

namespace ebox.EntityFrameworkCore.Seed.Host
{
    public class InitialEquipmentType
    {
        private readonly eboxDbContext _context;
        public InitialEquipmentType(eboxDbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            CreateEquipmentTypes();
        }
        private void CreateEquipmentTypes()
        {

            var eTypes = _context.EquipmentTypes.FirstOrDefault(x => x.Title == "Hand Tools");
            if (eTypes == null)
            {
                _context.EquipmentTypes.Add(new EquipmentType("Electrical","fa fa-plug"));
                _context.EquipmentTypes.Add(new EquipmentType("Powered","fa fa-tachometer"));
                _context.EquipmentTypes.Add(new EquipmentType("Mantainance","fa fa-wrench"));
                _context.EquipmentTypes.Add(new EquipmentType("Hand Tools","fa fa-signing"));
                _context.EquipmentTypes.Add(new EquipmentType("Machanical","fa fa-gears"));

                _context.SaveChanges();
            }
        }
    }
}