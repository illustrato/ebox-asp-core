﻿namespace ebox.EntityFrameworkCore.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly eboxDbContext _context;

        public InitialHostDbBuilder(eboxDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new InitialDepartments(_context).Create();
            new InitialEquipmentType(_context).Create();

            _context.SaveChanges();
        }
    }
}
