using System.Linq;
using ebox.Departments;

namespace ebox.EntityFrameworkCore.Seed.Host
{
    public class InitialDepartments
    {
        private readonly eboxDbContext _context;
        public InitialDepartments(eboxDbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            CreateDepartments();
        }
        private void CreateDepartments()
        {

            var eTypes = _context.Departments.FirstOrDefault(x => x.Id == "CL");
            if (eTypes == null)
            {
                _context.Departments.Add(new Department("CA","Catering","fa-cutlery"));
                _context.Departments.Add(new Department("CL","Cleaning","fa-diamond"));
                _context.Departments.Add(new Department("IC","ICT","fa-server"));
                _context.Departments.Add(new Department("MA","Maintainance","fa-wrench"));
                _context.Departments.Add(new Department("SE","Security","fa-key"));

                _context.SaveChanges();
            }
        }
    }
}