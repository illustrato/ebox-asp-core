using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Items.Dtos;

namespace ebox.Items {
    public interface IUnitAppService : IAsyncCrudAppService<UnitDto, ulong, PagedUnitResultRequestDto, CreateUnitInput, UnitDto> {
        Task Fit (EntityDto<ulong> input);
        Task Delegate (DelegateInputDto input);
    }
}