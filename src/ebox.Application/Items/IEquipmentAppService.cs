using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Items.Dtos;
using ebox.Sample.Dtos;

namespace ebox.Items {
    public interface IEquipmentAppService : IAsyncCrudAppService<EquipmentDto, Guid, PagedItemResultRequestDto, CreateEquipmentInput, EquipmentDto> {
        Task SetImage(Guid id, Base64Dto input);
        Task CreateType (CreateTypeInput input);
        Task UpdateType (CreateTypeInput input);
        Task DeleteType (EntityDto<byte> input);
        Task<ListResultDto<ItemTypeListDto>> GetTypes ();
    }
}