using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using ebox.Items.Dtos;
using ebox.Sample.Dtos;
using Microsoft.EntityFrameworkCore;

namespace ebox.Items {
    public class EquipmentAppService : AsyncCrudAppService<Equipment, EquipmentDto, Guid, PagedItemResultRequestDto, CreateEquipmentInput, EquipmentDto>, IEquipmentAppService, ITransientDependency {
        private readonly IEquipmentManager _itemManager;
        private readonly UnitManager _unitManager;
        private readonly IRepository<Unit, ulong> _unitRepo;
        private readonly IRepository<EquipmentType, byte> _typeRepo;
        public EquipmentAppService (
            IEquipmentManager itemManager,
            UnitManager unitManager,
            IRepository<Equipment, Guid> repository,
            IRepository<EquipmentType, byte> typeRepo,
            IRepository<Unit, ulong> unitRepo
        ) : base (repository) {
            _itemManager = itemManager;
            _unitManager = unitManager;
            _typeRepo = typeRepo;
            _unitRepo = unitRepo;
        }
        public override async Task<EquipmentDto> Create (CreateEquipmentInput input) {
            var @item = Equipment.Create (
                AbpSession.TenantId.Value,
                input.Title,
                input.TypeId,
                input.DepartmentId,
                input.Specs,
                input.Description);

            await _itemManager.CreateAsync(@item);

            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(@item);
        }
        protected override async Task<Equipment> GetEntityByIdAsync (Guid id) {
            return await Repository
            .GetAll ()
            .Include (e => e.EquipmentType)
            .Include (r => r.Units)
            .Where (e => e.Id == id)
            .FirstOrDefaultAsync ();
        }
        public async Task SetImage (Guid id, Base64Dto input) {
            var @item = await Repository.GetAsync (id);
            if (@item == null) {
                throw new UserFriendlyException ("failed locating equipment.");
            }
            if (input.Insert) {
                @item.SetImage ("png");
                FileSystemMediaProcessor.SaveImage (input.Base64, $"wwwroot/img/equipment/{AbpSession.TenantId.Value}", $"{id}.png");
            } else {
                @item.SetImage ();
                File.Delete ($"wwwroot/img/equipment/{AbpSession.TenantId.Value}/{id}.png");
            }
            await Repository.UpdateAsync (@item);
        }
        public async Task<EquipmentOutputDto> GetDetail (EntityDto<Guid> input) {
            var @item = await Repository
                .GetAll ()
                .Include (e => e.EquipmentType)
                .Include (r => r.Units)
                .Where (e => e.Id == input.Id)
                .FirstOrDefaultAsync ();

            if (@item == null) {
                throw new UserFriendlyException ("failed locating item.");
            }
            return ObjectMapper.Map<EquipmentOutputDto> (@item);
        }
        public override async Task<EquipmentDto> Update (EquipmentDto input) {
            var @item = await Repository.GetAsync (input.Id);
            if (@item == null) {
                throw new UserFriendlyException ("failed locating equipment");
            }

            MapToEntity(input, @item);

            return await Get(input);
        }
        public override async Task Delete (EntityDto<Guid> input) {
            var @emp = Repository.GetAsync (input.Id).Result;
            await SetImage (input.Id, new Base64Dto { Insert = false });
            await Repository.DeleteAsync (@emp);
        }
        public async Task<ListResultDto<ItemTypeListDto>> GetTypes () {
            var @types = await _typeRepo.GetAll ().Include (e => e.Equipments).ToListAsync ();
            return new ListResultDto<ItemTypeListDto> (ObjectMapper.Map<List<ItemTypeListDto>> (@types));
        }
        public async Task CreateType (CreateTypeInput input) {
            var @type = EquipmentType.Create (await GenerateId (), input.Title, input.Description);
            await _typeRepo.InsertAsync (@type);
        }
        public async Task DeleteType (EntityDto<byte> input) {
            var @type = await _typeRepo.GetAsync (input.Id);
            await _typeRepo.DeleteAsync (@type);
        }
        protected override IQueryable<Equipment> CreateFilteredQuery (PagedItemResultRequestDto input) {
                return Repository.GetAll ()
                    .Include (x => x.EquipmentType)
                    .Include (e => e.Units)
                    .WhereIf (!input.IncludeDeletedItems, e => !e.IsDeleted)
                    .WhereIf (input.TypeId.HasValue, e => e.TypeId == input.TypeId)
                    .WhereIf (!input.DepartmentId.IsNullOrWhiteSpace (), e => e.DepartmentId == input.DepartmentId)
                    .WhereIf (!input.Keyword.IsNullOrWhiteSpace () && input.Keyword != "null", x => x.Description.Contains (input.Keyword) || x.Specs.Contains (input.Keyword) || x.Title.Contains (input.Keyword))
                    .Skip (input.SkipCount)
                    .Take (input.MaxResultCount);
        }
        public async Task UpdateType (CreateTypeInput input) {
            var @type = await _typeRepo.GetAsync (input.Id);
            if (@type == null) {
                throw new UserFriendlyException ("failed locating equipment type");
            }
            ObjectMapper.Map (input, @type);
            await _typeRepo.UpdateAsync (@type);
        }
        private async Task<byte> GenerateId () {
            var ids = await _typeRepo.GetAll ().Select (e => e.Id).ToListAsync ();
            byte id = 1;
            while (ids.Contains (id)) {
                id++;
            }
            return id;
        }
        protected override Equipment MapToEntity (CreateEquipmentInput createInput) {
            var item = ObjectMapper.Map<Equipment> (createInput);
            return item;
        }
        protected override void MapToEntity (EquipmentDto input, Equipment item) {
            ObjectMapper.Map (input, item);
        }
        protected override EquipmentDto MapToEntityDto (Equipment item) {
            var itemDto = base.MapToEntityDto (item);            

            itemDto.AllUnits = _unitManager.Units.Where (r => item.Units.Any (ur => ur.Id == r.Id)).Select (r => r.ProductSerial).ToArray ();
            itemDto.UnitsInUse = _unitManager.Units.Where (r => item.Units.Any (ur => ur.Id == r.Id && ur.UserId.HasValue)).Select (r => r.ProductSerial).ToArray();
            itemDto.FaultyUnits = _unitManager.Units.Where (r => item.Units.Any (ur => ur.Id == r.Id && !ur.IsFit)).Select (r => r.ProductSerial).ToArray();

            return itemDto;
        }

    }
}