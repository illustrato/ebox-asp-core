using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Abp.Extensions;
using ebox.Items.Dtos;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;

namespace ebox.Items {
    public class UnitAppService : AsyncCrudAppService<Unit, UnitDto, ulong, PagedUnitResultRequestDto, CreateUnitInput, UnitDto>, IUnitAppService{
        private readonly IUnitManager _unitManager;

        public UnitAppService (
            IUnitManager unitManager,
            IRepository<Unit, ulong> unitRepo
        ):base(unitRepo) {
            _unitManager = unitManager;
        }
        public override async Task<UnitDto> Create (CreateUnitInput input) {
            ulong id = await _unitManager.GenerateId ();
            var @unit = Unit.Create (id, input.EquipmentId, input.Serial);
            await _unitManager.CreateAsync (@unit);
            return MapToEntityDto(@unit);
        }

        public async Task Delegate(DelegateInputDto input)
        {
            var @unit = await Repository.GetAsync(input.UnitId);
            if(@unit == null){
                throw new UserFriendlyException ("failed locating equipment.");
            }
            @unit.Delegate(input.UserId);
            await Repository.UpdateAsync(@unit);
        }

        public override async Task Delete (EntityDto<ulong> input) {
            var @unit = Repository.GetAsync (input.Id).Result;
            await Repository.DeleteAsync (@unit);
        }
        public async Task Fit (EntityDto<ulong> input) {
            var @unit = await Repository.GetAsync (input.Id);
            @unit.Fit (!@unit.IsFit);
            await Repository.UpdateAsync (@unit);
        }
        protected override async Task<Unit> GetEntityByIdAsync (ulong id) {
            return await Repository
                .GetAll ()
                .Include (e => e.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .Include( e => e.Employee)
                .FirstOrDefaultAsync (e => e.Id == id);
        }
        public async Task<UnitOutputDto> GetDetail (EntityDto<ulong> input) {
            var @unit = await Repository
                .GetAll ()
                .Include (e => e.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .Include( e => e.Employee)
                .Where (e => e.Id == input.Id)
                .FirstOrDefaultAsync ();

            if (@unit == null) {
                throw new UserFriendlyException ("failed locating equipment.");
            }
            return ObjectMapper.Map<UnitOutputDto> (@unit);
        }
        protected override IQueryable<Unit> CreateFilteredQuery(PagedUnitResultRequestDto input)
        {
            return  Repository
                .GetAll ()
                .Include(e => e.Employee)
                .ThenInclude(e => e.User)
                .WhereIf (input.EquipmentId.HasValue, e => e.EquipmentId == input.EquipmentId)
                .WhereIf (input.OnlyGoodCondition, e => e.IsFit)
                .WhereIf (!input.Keyword.IsNullOrWhiteSpace() && input.Keyword != "null", x => x.ProductSerial.Contains(input.Keyword) )
                .OrderByDescending (e => e.Date)
                .Take (input.MaxResultCount)
                .Skip(input.SkipCount);
        }
        public async Task<ListResultDto<UnitOutputDto>> GetList (PagedUnitResultRequestDto input) {
            var units = await Repository
                .GetAll ()
                .Include(e => e.Employee)
                .ThenInclude(e => e.User)
                .WhereIf (input.EquipmentId.HasValue, e => e.EquipmentId == input.EquipmentId)
                .WhereIf (input.OnlyGoodCondition, e => e.IsFit)
                .WhereIf (!input.Keyword.IsNullOrWhiteSpace() && input.Keyword != "null", x => x.ProductSerial.Contains(input.Keyword) )
                .OrderByDescending (e => e.Date)
                .Take (input.MaxResultCount)
                .Skip(input.SkipCount)
                .ToListAsync ();

            return new ListResultDto<UnitOutputDto> (ObjectMapper.Map<List<UnitOutputDto>> (units));
        }

        public async Task Revoke(EntityDto<ulong> input)
        {
            var @unit = await Repository.GetAsync(input.Id);
            if(@unit == null){
                throw new UserFriendlyException ("failed locating equipment.");
            }
            @unit.Delegate(null);
            await Repository.UpdateAsync(@unit);
        }
    }
}