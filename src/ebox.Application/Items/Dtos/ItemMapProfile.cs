using AutoMapper;

namespace ebox.Items.Dtos {
    public class ItemMapProfile : Profile {
        public ItemMapProfile () {
            CreateMap<CreateTypeInput, EquipmentType> ();

            CreateMap<EquipmentDto, Equipment>();
            CreateMap<EquipmentDto, Equipment>()
                .ForMember(x => x.Units, opt => opt.Ignore())
                .ForMember(x => x.CreationTime, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.EquipmentType, opt => opt.Ignore());

            CreateMap<CreateEquipmentInput, Equipment>();
            CreateMap<CreateEquipmentInput, Equipment>().ForMember(x => x.Units, opt => opt.Ignore());
        }
    }
}