using Abp.Application.Services.Dto;

namespace ebox.Items.Dtos {
    public class PagedItemResultRequestDto : PagedResultRequestDto {
        public string Keyword { get; set; }
        public bool IncludeDeletedItems { get; set; }
        public byte? TypeId { get; set; }
        public string DepartmentId { get; set; }
    }
}