using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (EquipmentType))]
    public class ItemTypeListDto : EntityDto<byte> {
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual int EquipmentsCount { get; protected set; }
    }
}