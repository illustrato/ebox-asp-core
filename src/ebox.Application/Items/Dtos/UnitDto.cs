using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Employees;
using ebox.Employees.Dtos;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (Unit))]
    public class UnitDto : EntityDto<ulong> {
        public Guid EquipmentId { get; set; }
        public DateTime Date { get; set; }
        public EmployeeDto Employee { get; set;}
        public long? UserId {get; set;}
        public bool IsFit { get; set; }
        public string ProductSerial { get; set; }
    }
}