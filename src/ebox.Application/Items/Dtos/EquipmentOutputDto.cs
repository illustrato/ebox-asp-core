using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Departments;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (Equipment))]
    public class EquipmentOutputDto : FullAuditedEntityDto<Guid> {
        public string Title { get; set; }
        public string Specs { get; set; }
        public string Description { get; set; }
        public EquipmentTypeDto EquipmentType { get; set; }
        public byte TypeId { get; set; }
        public string DepartmentId { get; set; }
        public string Image { get; set; }
        public int UnitsCount { get; set; }
        public ICollection<UnitDto> Units { get; set; }
    }
}