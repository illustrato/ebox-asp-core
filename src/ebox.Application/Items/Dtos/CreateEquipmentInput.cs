using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Items.Dtos {
    
    [AutoMapTo(typeof(Equipment))]
    public class CreateEquipmentInput {
        [Required]
        [StringLength (Equipment.MaxTitleLength)]
        public string Title { get; set; }
        [StringLength (Equipment.MaxDescriptionLength)]
        public string Specs { get; set; }

        [StringLength (Equipment.MaxDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public byte TypeId { get; set; }

        [Required]
        public string DepartmentId { get; set; }
    }
}