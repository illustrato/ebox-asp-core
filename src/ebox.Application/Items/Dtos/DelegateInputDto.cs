namespace ebox.Items.Dtos
{
    public class DelegateInputDto
    {
        public long UserId { get; set; }
        public ulong UnitId { get; set; }
    }
}