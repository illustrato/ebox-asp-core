using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (Equipment))]
    public class EquipmentDto : EntityDto<Guid> {
        [Required]
        [StringLength (Equipment.MaxTitleLength)]
        public string Title { get; set; }
        [StringLength (Equipment.MaxDescriptionLength)]
        public string Specs { get; set; }

        [StringLength (Equipment.MaxDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public byte TypeId { get; set; }

        public EquipmentTypeDto EquipmentType { get; set;}
        public DateTime CreationTime { get; set; }
        public bool IsDeleted { get; set; }
        public string DepartmentId { get; set; }
        public string Image { get; set; }
        public string[] AllUnits { get; set; }
        public string[] UnitsInUse { get; set; }
        public string[] FaultyUnits { get; set; }
    }
}