using System;
using Abp.Application.Services.Dto;

namespace ebox.Items.Dtos {
    public class PagedUnitResultRequestDto : PagedResultRequestDto {
        public string Keyword { get; set; }
        public bool OnlyGoodCondition { get; set; }
        public Guid? EquipmentId { get; set; }
    }
}