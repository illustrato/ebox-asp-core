using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Employees.Dtos;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (Unit))]
    public class UnitOutputDto : EntityDto<ulong> {

        public EmployeeDto Employee { get; set;}
        public EquipmentDto Equipment { get; set;}
        public DateTime Date { get; set; }
        public DateTime? UsageDate { get; set; }
        public long? UserId {get; set;}
        public bool IsFit { get; set; }
        public string ProductSerial { get; set; }
    }
}