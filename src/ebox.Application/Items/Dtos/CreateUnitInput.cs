using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace ebox.Items.Dtos {
    [AutoMapTo(typeof(Unit))]
    public class CreateUnitInput {

        [Required]
        public Guid EquipmentId { get; set; }
        [Required]
        public string Serial { get; set; }
    }
}