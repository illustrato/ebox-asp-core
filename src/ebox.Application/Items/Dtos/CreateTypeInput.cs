using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Items.Dtos {
    [AutoMapFrom (typeof (EquipmentType))]
    public class CreateTypeInput : EntityDto<byte> {
        [Required]
        [StringLength (EquipmentType.MaxTitleLength)]
        public string Title { get; set; }

        [StringLength (EquipmentType.MaxDescriptionLength)]
        public string Description { get; set; }
        [StringLength (EquipmentType.MaxTitleLength)]
        public string FontIcon { get; set;}
    }
}