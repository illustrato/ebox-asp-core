using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Items;

namespace ebox.Stats.Dtos
{
    [AutoMapFrom (typeof (Unit))]
    public class NewItemsListDto : EntityDto<ulong>
    {
        public DateTime Date { get; set; }
    }
}