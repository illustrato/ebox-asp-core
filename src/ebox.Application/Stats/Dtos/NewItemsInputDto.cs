using System;

namespace ebox.Stats.Dtos
{
    public class NewItemsInputDto
    {
        public DateTime? StartDate { get; set;}
        public DateTime? EndDate { get; set;}
        public byte? TypeId { get; set; }
        public string DepartmentId { get; set; }
    }
}