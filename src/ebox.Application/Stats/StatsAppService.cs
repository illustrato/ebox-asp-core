using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using ebox.Items;
using ebox.Stats.Dtos;
using System.Collections.Generic;

namespace ebox.Stats
{
    public class StatsAppService : eboxAppServiceBase, IStatsAppService 
    {
        private readonly IRepository<Unit, ulong> _unitRepo;

        public StatsAppService(
            IRepository<Unit, ulong> unitRepo
        ){
            _unitRepo = unitRepo;
        }

        public async Task<ListResultDto<NewItemsListDto>> GetNewItems(NewItemsInputDto input)
        {
            var sql = "SELECT CONVERT(DATE, DATE) DATE, CAST(COUNT(ID) AS DECIMAL(13,5)) ID, CAST(5 AS BIGINT) USERID, GETDATE() USAGEDATE, ";
            sql += "(SELECT TOP 1 ISFIT FROM abp_equipment_unit) ISFIT, (SELECT top 1 EQUIPMENTID FROM abp_equipment_unit) EQUIPMENTID, ";
            sql += "'' PRODUCTSERIAL  FROM abp_equipment_unit GROUP BY CONVERT(DATE, DATE)";
            
            var @relation = await _unitRepo.GetAll().FromSql(sql).AsNoTracking().ToListAsync();
            
            return new ListResultDto<NewItemsListDto> (ObjectMapper.Map<List<NewItemsListDto>> (@relation));
        }
    }
}