using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Employees.Dtos;
using ebox.Items.Dtos;

namespace ebox.Employees
{
    public interface IEmployeeAppService : IApplicationService
    {
         Task Delete(EntityDto<long> input);
         Task<EmployeeDetailOutputDto> GetDetail(long id);
         Task<ListResultDto<UnitOutputDto>> GetEquipment(UnitResultFilterDto input);
         Task<ListResultDto<ManagerDto>> GetManagerList();
         Task<ListResultDto<SupervisorDto>> GetSupervisorList();
         Task<ListResultDto<WorkerDto>> GetWorkerList();
    }
}