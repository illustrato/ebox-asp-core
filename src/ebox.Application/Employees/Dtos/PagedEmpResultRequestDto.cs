using Abp.Application.Services.Dto;

namespace ebox.Employees.Dtos
{
    public class PagedEmpResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public bool? IsActive { get; set; }

        public string DepartmentId { get; set; }
    }
}