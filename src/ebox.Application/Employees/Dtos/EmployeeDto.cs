using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Items.Dtos;
using ebox.Users.Dto;

namespace ebox.Employees.Dtos
{
    [AutoMapFrom(typeof(Employee))]
    public class EmployeeDto : EntityDto<long>
    {
        public string IdNo { get; set; }
        public string DepartmentId { get; set; }
        public UserDto User { get; set; }
        public int UnitsCount { get;  set; }
        public int FaultsCount { get; set; }
    }
}