using Abp.AutoMapper;
using Abp.Runtime.Validation;
using ebox.Users.Dto;

namespace ebox.Employees.Dtos
{
    [AutoMapTo(typeof(Employee))]
    public class CreateEmployeeDto : IShouldNormalize
    {
        public string IdNo { get; set; }
        public string DepartmentId { get; set; }
        public CreateUserDto User { get; set; }

        public void Normalize()
        {
            if (User.RoleNames == null)
            {
                User.RoleNames = new string[0];
            }
        }
    }
}