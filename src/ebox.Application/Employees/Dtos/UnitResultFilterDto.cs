namespace ebox.Employees.Dtos
{
    public class UnitResultFilterDto
    {
        public string Keyword { get; set; }
        public byte? Type { get; set; }

        public long UserId { get; set; }
    }
}