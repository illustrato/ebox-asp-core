using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Authorization.Users;
using ebox.Users.Dto;

namespace ebox.Employees.Dtos
{
    [AutoMapFrom(typeof(Supervisor))]
    public class SupervisorDto : EntityDto<long>
    {
        public EmployeeDto Employee { get; set; }
    }
}