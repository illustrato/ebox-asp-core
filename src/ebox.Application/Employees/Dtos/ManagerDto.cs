using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Authorization.Users;
using ebox.Users.Dto;

namespace ebox.Employees.Dtos
{
    [AutoMapFrom(typeof(Manager))]
    public class ManagerDto : EntityDto<long>
    {
        public EmployeeDto Employee { get; set; }
    }
}