using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.UI;
using ebox.Authorization.Roles;
using ebox.Authorization.Users;
using ebox.Employees.Dtos;
using ebox.Items;
using ebox.Items.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ebox.Employees {
    public class EmployeeAppService : AsyncCrudAppService<Employee, EmployeeDto, long, PagedEmpResultRequestDto, CreateEmployeeDto, EmployeeDto>, IEmployeeAppService {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IEmployeeManager _empManager;
        private readonly IRepository<Worker, long> _wrkRepo;
        private readonly IRepository<Manager, long> _mngrRepo;
        private readonly IRepository<Supervisor, long> _spvrRepo;
        private readonly IRepository<User, long> _userRepo;
        private readonly IRepository<Unit, ulong> _unitRepo;
        private readonly UserRegistrationManager _userRegistrationManager;
        public EmployeeAppService (
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Employee, long> repository,
            EmployeeManager empManager,

            IRepository<Worker, long> wrkRepo,
            IRepository<Manager, long> mngrRepo,
            IRepository<Supervisor, long> spvrRepo,
            IRepository<User, long> userRepository,
            IRepository<Unit, ulong> unitRepo
        ) : base (repository) {
            _roleManager = roleManager;
            _empManager = empManager;

            _userManager = userManager;
            _userRepo = userRepository;
            _mngrRepo = mngrRepo;
            _spvrRepo = spvrRepo;
            _wrkRepo = wrkRepo;

            _unitRepo = unitRepo;
        }

        public override async Task<EmployeeDto> Create (CreateEmployeeDto input) {
            CheckCreatePermission ();

            var emp = ObjectMapper.Map<Employee> (input);

            emp.User.TenantId = AbpSession.TenantId;
            emp.User.IsEmailConfirmed = true;

            await _userManager.InitializeOptionsAsync (emp.User.TenantId);

            CheckErrors (await _userManager.CreateAsync (emp.User, User.DefaultPassword));

            CurrentUnitOfWork.SaveChanges ();

            if (input.User.RoleNames != null) {
                CheckErrors (await _userManager.SetRoles (emp.User, input.User.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges ();

            await Repository.InsertAsync (Employee.Create(emp.User.Id,emp.IdNo,emp.DepartmentId));

            return MapToEntityDto (emp);
        }

        public override async Task Delete (EntityDto<long> input) {
            var @emp = await _userManager.GetUserByIdAsync (input.Id);
            if (@emp == null) {
                throw new UserFriendlyException ("failed locating employee");
            }
            await _userManager.DeleteAsync (@emp);
        }

        [UnitOfWork]
        protected override IQueryable<Employee> CreateFilteredQuery (PagedEmpResultRequestDto input) {
            using (CurrentUnitOfWork.DisableFilter (AbpDataFilters.MustHaveTenant, AbpDataFilters.MayHaveTenant)) {
                return Repository.GetAll ()
                    .Include (x => x.User)
                    .ThenInclude (x => x.Roles)
                    .WhereIf (!input.DepartmentId.IsNullOrWhiteSpace (), x => x.DepartmentId == input.DepartmentId)
                    .WhereIf (!input.Keyword.IsNullOrWhiteSpace (), x => x.User.UserName.Contains (input.Keyword) || x.User.Name.Contains (input.Keyword) || x.User.EmailAddress.Contains (input.Keyword))
                    .WhereIf (input.IsActive.HasValue, x => x.User.IsActive == input.IsActive)
                    .Skip (input.SkipCount)
                    .Take (input.MaxResultCount).AsQueryable ();
            }
        }

        [UnitOfWork]
        protected override async Task<Employee> GetEntityByIdAsync (long id) {
            using (CurrentUnitOfWork.DisableFilter (AbpDataFilters.MustHaveTenant, AbpDataFilters.MayHaveTenant)) {
                var employee = await Repository.GetAll ()
                    .Include (x => x.User)
                    .ThenInclude (x => x.Roles)
                    .Include (e => e.Units)
                    .Include (e => e.Faults)
                    .FirstOrDefaultAsync (x => x.Id == id);

                if (employee == null) {
                    throw new EntityNotFoundException (typeof (Employee), id);
                }
                return employee;
            }
        }
        public async Task<EmployeeDetailOutputDto> GetDetail (long id) {
            var employee = await Repository.GetAll ()
                .Include (x => x.User)
                .ThenInclude (x => x.Roles)
                .Include (e => e.Units)
                .Include (e => e.Faults)
                .FirstOrDefaultAsync (x => x.Id == id);

            if (employee == null) {
                return null;
            }
            return ObjectMapper.Map<EmployeeDetailOutputDto> (employee);
        }

        public async Task<ListResultDto<UnitOutputDto>> GetEquipment (UnitResultFilterDto input) {
            var @items = await _unitRepo.GetAll()
            .Include(e => e.Equipment)
            .ThenInclude(x => x.EquipmentType)
            .Where (x => x.UserId == input.UserId)
            .WhereIf (!input.Keyword.IsNullOrWhiteSpace (), x => x.ProductSerial.Contains (input.Keyword) || x.Equipment.Title.Contains (input.Keyword) || x.Equipment.Specs.Contains (input.Keyword))
            .WhereIf (input.Type.HasValue, x => x.Equipment.TypeId == input.Type)
            .OrderByDescending (x => x.UsageDate)
            .ToListAsync ();
            return new ListResultDto<UnitOutputDto> (ObjectMapper.Map<List<UnitOutputDto>> (@items));
        }
        public async Task<ListResultDto<ManagerDto>> GetManagerList () {
            var employees = await _mngrRepo
                .GetAll ()
                .Include (e => e.Employee)
                .ThenInclude (m => m.User)
                .OrderByDescending (e => e.Employee.IdNo)
                .Take (64)
                .ToListAsync ();

            return new ListResultDto<ManagerDto> (ObjectMapper.Map<List<ManagerDto>> (employees));
        }
        public async Task<ListResultDto<SupervisorDto>> GetSupervisorList () {
            var employees = await _spvrRepo
                .GetAll ()
                .Include (e => e.Employee)
                .ThenInclude (m => m.User)
                .OrderByDescending (e => e.Employee.IdNo)
                .Take (64)
                .ToListAsync ();

            return new ListResultDto<SupervisorDto> (ObjectMapper.Map<List<SupervisorDto>> (employees));
        }

        public async Task<ListResultDto<WorkerDto>> GetWorkerList () {
            var employees = await _wrkRepo
                .GetAll ()
                .Include (e => e.Employee)
                .ThenInclude (m => m.User)
                .OrderByDescending (e => e.Employee.IdNo)
                .Take (64)
                .ToListAsync ();

            return new ListResultDto<WorkerDto> (ObjectMapper.Map<List<WorkerDto>> (employees));
        }

        public override async Task<EmployeeDto> Update (EmployeeDto input) {
            CheckUpdatePermission ();

            var user = await _userManager.GetUserByIdAsync (input.Id);

            ObjectMapper.Map (input.User, user);
            user.SetNormalizedNames ();

            CheckErrors (await _userManager.UpdateAsync (user));

            if (input.User.RoleNames != null) {
                CheckErrors (await _userManager.SetRoles (user, input.User.RoleNames));
            }

            // user = await _userRepo.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == input.User.Id);

            //  UPDATE User Acount
            var @emp = await Repository.GetAsync (input.Id);
            if (@emp == null) {
                throw new UserFriendlyException ("failed locating employee");
            }
            MapToEntity (input, @emp);
            await Repository.UpdateAsync (@emp);

            return await Get (input);
        }
        protected override Employee MapToEntity (CreateEmployeeDto createInput) {
            var emp = ObjectMapper.Map<Employee> (createInput);
            emp.User.SetNormalizedNames ();
            return emp;
        }
        protected override void MapToEntity (EmployeeDto input, Employee emp) {
            emp.Edit (input.DepartmentId, input.IdNo);
            // ObjectMapper.Map(input, emp);
            emp.User.SetNormalizedNames ();
        }
        protected override EmployeeDto MapToEntityDto (Employee emp) {
            var roles = _roleManager.Roles.Where (r => emp.User.Roles.Any (ur => ur.RoleId == r.Id)).Select (r => r.NormalizedName);
            var empDto = base.MapToEntityDto (emp);
            empDto.User.RoleNames = roles.ToArray ();
            return empDto;
        }

        protected virtual void CheckErrors (IdentityResult identityResult) {
            identityResult.CheckErrors (LocalizationManager);
        }
    }
}