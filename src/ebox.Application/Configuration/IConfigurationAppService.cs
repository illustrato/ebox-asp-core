﻿using System.Threading.Tasks;
using ebox.Configuration.Dto;

namespace ebox.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
