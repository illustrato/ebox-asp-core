using System;
using System.IO;
using System.Threading.Tasks;
using Abp.UI;

namespace ebox {
    public class FileSystemMediaProcessor {
        //  ------------------------------------------------------  from Base 64
        public static void SaveImage (string base64, string location, string file) {
            //Convert Base64 Encoded string to Byte Array
            byte[] imageBytes = ConvertToBytes (base64);
            //Save the Byte Array as Image File.
            EnsureFsPath (location);
            var fsPath = $"{location}/{file}".Replace ('/', Path.DirectorySeparatorChar);
            File.WriteAllBytes (fsPath, imageBytes);
        }
        protected static void EnsureFolderPaths (string existingPath, string[] folderNameSegments) {
            if (string.IsNullOrEmpty (existingPath)) return;
            if (folderNameSegments.Length == 0) return;
            if (!Directory.Exists (existingPath)) return;

            var partial = existingPath;
            for (var i = 0; i < folderNameSegments.Length; i++) {
                partial = Path.Combine (partial, folderNameSegments[i]);

                if (!Directory.Exists (partial)) {
                    try {
                        Directory.CreateDirectory (partial);

                    } catch (Exception ex) {
                        throw new UserFriendlyException ($"failed to create folder {partial}", ex);

                    }
                }
            }
        }
        private static void EnsureFsPath (string mediaVirtualPath) {
            var fsPath = Environment.CurrentDirectory + "/" + mediaVirtualPath.Replace ('/', Path.DirectorySeparatorChar);
            if (Directory.Exists (fsPath)) return; //nothing to do

            var segments = mediaVirtualPath.Split ('/');

            EnsureFolderPaths (Environment.CurrentDirectory, segments);
        }
        private static byte[] ConvertToBytes (string base64) {
            int index = base64.IndexOf ("base64,", StringComparison.Ordinal) + 7;
            return Convert.FromBase64String (base64.Substring (index));
        }

        //  -------------------------------------------------------------   to Base 64
        public static string ReadImage (string file, string type = "png") {
            byte[] imageArray = File.ReadAllBytes (file.Replace ('/', Path.DirectorySeparatorChar));
            return $"data:image/{type};base64," + Convert.ToBase64String(imageArray);
        }
    }
}