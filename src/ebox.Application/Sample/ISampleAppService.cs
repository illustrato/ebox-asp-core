using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Sample.Dtos;

namespace ebox.Sample
{
    public interface ISampleAppService : IApplicationService
    {
        EntityDto<string> GetBase64(EntityDto<string> input);
    }
}