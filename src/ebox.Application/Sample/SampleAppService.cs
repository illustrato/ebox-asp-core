using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using ebox.Sample.Dtos;

namespace ebox.Sample
{
    public class SampleAppService : eboxAppServiceBase, ISampleAppService
    {
        public EntityDto<string> GetBase64(EntityDto<string> input)
        {
            input.Id = FileSystemMediaProcessor.ReadImage(input.Id);
            return input;
        }
    }
}