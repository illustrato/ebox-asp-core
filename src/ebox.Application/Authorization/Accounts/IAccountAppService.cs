﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ebox.Authorization.Accounts.Dto;

namespace ebox.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
