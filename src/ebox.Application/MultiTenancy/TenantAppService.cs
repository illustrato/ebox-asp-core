﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using Abp.UI;
using ebox.Authorization;
using ebox.Authorization.Roles;
using ebox.Authorization.Users;
using ebox.Editions;
using ebox.MultiTenancy.Dto;
using Microsoft.AspNetCore.Identity;

namespace ebox.MultiTenancy
{
    // [AbpAuthorize(PermissionNames.Pages_Tenants)]
    public class TenantAppService : AsyncCrudAppService<Tenant, TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>, ITenantAppService
    {
        private readonly TenantManager _tenantManager;
        private readonly EditionManager _editionManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;

        private readonly IRepository<Admin,long> _adminRepo;
        private readonly IRepository<AdminTenant,string> _adminTenantRepo;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<UserRole,long> _userRoles;

        public TenantAppService(
            IRepository<Tenant, int> repository,
            TenantManager tenantManager,
            EditionManager editionManager,
            UserManager userManager,
            RoleManager roleManager,
            IAbpZeroDbMigrator abpZeroDbMigrator,
            IRepository<Admin,long> adminRepo,
            IRepository<AdminTenant,string> adminTenantRepo,
            IPasswordHasher<User> passwordHasher,
            IRepository<UserRole,long> userRoles
            )
            : base(repository)
        {
            _tenantManager = tenantManager;
            _editionManager = editionManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _abpZeroDbMigrator = abpZeroDbMigrator;
            _adminRepo = adminRepo;
            _adminTenantRepo = adminTenantRepo;
            _passwordHasher = passwordHasher;
            _userRoles = userRoles;
        }

        public override async Task<TenantDto> Create(CreateTenantDto input)
        {
            CheckCreatePermission();

            await _userManager.CheckDuplicateUsernameOrEmailAddressAsync(null,input.AdminUserName,input.AdminEmailAddress);

            // Create tenant
            var tenant = ObjectMapper.Map<Tenant>(input);
            tenant.ConnectionString = input.ConnectionString.IsNullOrEmpty()
                ? null
                : SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

            var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
            if (defaultEdition != null)
            {
                tenant.EditionId = defaultEdition.Id;
            }

            await _tenantManager.CreateAsync(tenant);
            await CurrentUnitOfWork.SaveChangesAsync(); // To get new tenant's id.

            // Create tenant database
            _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

            //
            Role adminRole;
            User adminUser;

            // We are working entities of new tenant, so changing tenant filter
            using (CurrentUnitOfWork.SetTenantId(tenant.Id))
            {
                // Create static roles for new tenant
                CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));

                await CurrentUnitOfWork.SaveChangesAsync(); // To get static role ids

                // Grant all permissions to admin role
                adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
                await _roleManager.GrantAllPermissionsAsync(adminRole);

                // Create admin user for the tenant
                adminUser = User.CreateAdmin(input.AdminUserName, input.AdminEmailAddress);
                string password = input.Password.Trim().Length < 1 ? User.DefaultPassword : input.Password;
                adminUser.Password = _passwordHasher.HashPassword(adminUser,password);
                await _userManager.InitializeOptionsAsync(tenant.Id);
                CheckErrors(await _userManager.CreateAsync(adminUser, password));
                await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

                // Assign admin user to role!
                CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));
                await CurrentUnitOfWork.SaveChangesAsync();

                await _adminRepo.InsertAsync(Admin.Create(adminUser.Id));
                await _adminTenantRepo.InsertAsync(AdminTenant.Create(adminUser.Id,tenant.Id));

                //  remove tenant Id
                // var @user = await _userManager.GetUserByIdAsync(adminUser.Id);
                // @user.TenantId = null;
                // await _userManager.UpdateAsync(@user);
                
            }
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                // Assign admin user to role!
                CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            return MapToEntityDto(tenant);
        }

        protected override IQueryable<Tenant> CreateFilteredQuery(PagedTenantResultRequestDto input)
        {
            var @tenantIds = _adminTenantRepo.GetAll()
            .Where(e => e.UserId == AbpSession.UserId)
            .Select(e => e.TenantId)
            .ToArray();
            
            return Repository.GetAll()
            .Where(e => e.Id.IsIn(@tenantIds))
            .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.TenancyName.Contains(input.Keyword) || x.Name.Contains(input.Keyword))
            .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
        }

        protected override void MapToEntity(TenantDto updateInput, Tenant entity)
        {
            // Manually mapped since TenantDto contains non-editable properties too.
            entity.Name = updateInput.Name;
            entity.TenancyName = updateInput.TenancyName;
            entity.IsActive = updateInput.IsActive;
        }

        public override async Task Delete(EntityDto<int> input)
        {
            CheckDeletePermission();

            var tenant = await _tenantManager.GetByIdAsync(input.Id);
            await _tenantManager.DeleteAsync(tenant);
        }

        private void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}

