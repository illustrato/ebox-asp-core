﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.MultiTenancy.Dto;

namespace ebox.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

