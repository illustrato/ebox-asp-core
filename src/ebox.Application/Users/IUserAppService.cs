using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Roles.Dto;
using ebox.Users.Dto;

namespace ebox.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
