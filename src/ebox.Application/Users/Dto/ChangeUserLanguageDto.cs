using System.ComponentModel.DataAnnotations;

namespace ebox.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}