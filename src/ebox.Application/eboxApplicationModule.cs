﻿using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ebox.Authorization;

namespace ebox
{
    [DependsOn(
        typeof(eboxCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class eboxApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<eboxAuthorizationProvider>();
            Configuration.UnitOfWork.OverrideFilter(AbpDataFilters.SoftDelete, false);
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(eboxApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
