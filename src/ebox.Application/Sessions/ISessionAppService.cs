﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ebox.Sessions.Dto;

namespace ebox.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
