using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ebox.Departments.Dtos;

namespace ebox.Departments
{
    public interface IDepartmentAppService : IApplicationService
    {
        Task<List<Department>> GetAll();
        Task<ListResultDto<DeptListDto>> GetList(EntityDto<string> exception);
        Task<DeptDetailOutput> GetDetail(EntityDto<string> input);
        Task<DeptDetailOutput> Get(string id);
        Task<string> Create(AddDepartmentInput input);
        Task Update(AddDepartmentInput input);
        Task Delete(EntityDto<string> input);
        Task<ListResultDto<OrgDeptListDto>> GetAssociation(GetDeptListInput input);
        Task TerminateRegistration(EntityDto<long> input);
        Task RegisterEmployee(DeptRegistraInput input);
    }
}