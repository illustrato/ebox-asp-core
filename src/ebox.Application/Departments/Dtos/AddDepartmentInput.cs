using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Departments.Dtos {
    
    [AutoMapTo(typeof(OrgDept))]
    public class AddDepartmentInput : EntityDto<string> {
        [Required]
        public int TenantId { get; set; }

        [Required]
        public string DepartmentId { get; set; }

        public string Description { get; set; }
    }
}