using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using ebox.Employees;
using ebox.Employees.Dtos;
using ebox.Items.Dtos;
using ebox.MultiTenancy;

namespace ebox.Departments.Dtos
{
    [AutoMapFrom(typeof(OrgDept))]
    public class DeptDetailOutput : EntityDto<string>
    {

        public int TenantId { get; set; }
        public DepartmentDto Department { get; set; }
        public string Description { get;  set; }
        public int EmployeesCount { get; set; }
        public ICollection<EmployeeDto> Employees { get; set; }
        public int EquipmentCount {get; set;}
        public ICollection<EquipmentDto> Equipment {get; set;}
    }
}