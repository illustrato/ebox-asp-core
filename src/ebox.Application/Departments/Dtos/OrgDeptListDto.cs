using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Departments.Dtos
{
     [AutoMapFrom(typeof(OrgDept))]
    public class OrgDeptListDto : EntityDto<string>
    {
        public int TenantId { get; set; }
        public  DepartmentDto Department { get; set; }
        public  string Description { get; set; }
        public bool isDeleted { get; set; }
        public int EmployeesCount {get; set;}
        public int EquipmentCount {get; set;}
    }
}