using System.ComponentModel.DataAnnotations;

namespace ebox.Departments.Dtos
{
    public class DeptRegistraInput
    {
         [Required]
        public long UserId { get; set; }

        [Required]
        public string DepartmentId { get; set; }
    }
}