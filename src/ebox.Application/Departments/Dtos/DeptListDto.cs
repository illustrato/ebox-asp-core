using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace ebox.Departments.Dtos
{
     [AutoMapFrom(typeof(Department))]
    public class DeptListDto : FullAuditedEntityDto<string>
    {
        [StringLength(Department.MaxTitleLength)]
        public string DeptName { get; set;}

        [StringLength(Department.MaxTitleLength)]
        public  string FontIcon { get; set;}
        public int OrganizationsCount { get; set; }
    }
}