using Abp.Application.Services.Dto;

namespace ebox.Departments.Dtos
{
    public class GetDeptListInput 
    {
        public bool IncludeDeletedDepartments { get; set; }
    }
}