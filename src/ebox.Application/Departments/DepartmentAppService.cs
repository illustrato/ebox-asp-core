using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using ebox.Departments.Dtos;
using ebox.Employees;
using ebox.MultiTenancy;
using Microsoft.EntityFrameworkCore;

namespace ebox.Departments
{
    public class DepartmentAppService : eboxAppServiceBase, IDepartmentAppService
    {
        private readonly IRepository<Department, string> _deptRepo;
        private readonly IRepository<OrgDept, string> _orgDeptRepo;
        private readonly IRepository<Tenant, int> _tenantRepo;
        private readonly IRepository<Employee, long> _empRepo;
        public DepartmentAppService(
            IRepository<Department, string> deptRepo,
            IRepository<OrgDept, string> orgDeptRepo,
            IRepository<Tenant, int> tenantRepo,
            IRepository<Employee, long> empRepo
        ){
            _deptRepo = deptRepo;
            _orgDeptRepo = orgDeptRepo;
            _tenantRepo = tenantRepo;
            _empRepo = empRepo;
        }
        public async Task<DeptDetailOutput> Get(string id){
            var @dept = await _orgDeptRepo.GetAllIncluding(e => e.Department).FirstOrDefaultAsync(e => e.Id == id);
            return ObjectMapper.Map<DeptDetailOutput>(@dept);
        }
        public async Task<DeptDetailOutput> GetDetail(EntityDto<string> input)
        {
            var detail = await _orgDeptRepo
                .GetAll()
                .Include(e => e.Employees)
                .ThenInclude(r => r.User)
                .Include(e => e.Tenant)
                .Include(e => e.Department)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            if (detail == null)
            {
                throw new UserFriendlyException("Could not locate Department, maybe it's deleted.");
            }

            return ObjectMapper.Map<DeptDetailOutput>(detail);
        }

        public async Task<string> Create(AddDepartmentInput input)
        {
            var @item = await _orgDeptRepo.GetAll()
            .Where(e => e.Id == input.DepartmentId+AbpSession.TenantId.Value)
            .FirstOrDefaultAsync();

            if(@item == null){
                var @entry = OrgDept.Create(AbpSession.TenantId.Value,input.DepartmentId,input.Description);
            return await _orgDeptRepo.InsertAndGetIdAsync(@entry);
            }
            ObjectMapper.Map(input, @item);
            @item.Redeem(input.DepartmentId,AbpSession.TenantId.Value);
            await _orgDeptRepo.UpdateAsync(@item);
            return @item.Id;
        }

        public async Task Delete(EntityDto<string> input)
        {
            var @target = await _orgDeptRepo.GetAsync(input.Id);
            if(@target == null){
                throw new UserFriendlyException("failed locating department");
            }
            @target.Delete(AbpSession.UserId);
            await _orgDeptRepo.UpdateAsync(@target);
        }

        public async Task<List<Department>> GetAll()
        {
            return await _deptRepo.GetAllListAsync();
        }
        public async Task TerminateRegistration(EntityDto<long> input){
            var @emp = await _empRepo.GetAsync(input.Id);
            @emp.TerminateRegistration();
            await _empRepo.UpdateAsync(@emp);
        }

        public async Task<ListResultDto<DeptListDto>> GetList(EntityDto<string> exception = null)
        {
            var @deptIds = _orgDeptRepo.GetAll()
            .Where(e => !e.IsDeleted)
            .Select(e => e.DepartmentId)
            .ToArray();
            
            var @departments = await _deptRepo.GetAll()
            .Where(e => !e.Id.IsIn(@deptIds) || (exception != null && e.Id == exception.Id))
            .ToListAsync();
            return new ListResultDto<DeptListDto>(ObjectMapper.Map<List<DeptListDto>>(@departments));
        }
        public  Task<ListResultDto<OrgDeptListDto>> GetAssociation(GetDeptListInput input){
            var @list = _orgDeptRepo.GetAll()
            .Include(e => e.Department)
            .Include(e => e.Employees)
            .Include(e => e.Equipment)
            .Where(e => !e.IsDeleted || input.IncludeDeletedDepartments)
            .ToList();
            return Task.FromResult(new ListResultDto<OrgDeptListDto>(
                    ObjectMapper.Map<List<OrgDeptListDto>>(@list).ToList()
                ));
        }

        public async Task RegisterEmployee(DeptRegistraInput input)
        {
            var @emp = await _empRepo.GetAsync(input.UserId);
            @emp.RegisterDepartment(input.DepartmentId);
            await _empRepo.UpdateAsync(@emp);
        }

        public async Task Update(AddDepartmentInput input)
        {
            var @target = await _orgDeptRepo.GetAsync(input.Id);
            if (@target == null)
            {
                throw new UserFriendlyException("failed locating department");
            }
            ObjectMapper.Map(input, @target);
            await _orgDeptRepo.UpdateAsync(@target);
        }
        protected  OrgDept MapToEntity(AddDepartmentInput createInput)
        {
            var dept = ObjectMapper.Map<OrgDept>(createInput);
            return dept;
        }
    }
}